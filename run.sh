#!/bin/bash

sudo apt install qemu-system-i386 nasm
nasm -f elf32 kernel.asm -o kasm.o
make clean
make
qemu-system-i386 -kernel kernel