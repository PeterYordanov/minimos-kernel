bits 32

; Setting flags, magic number and checksum

section .text
        ;multiboot spec
        align 4
        dd 0x1BADB002
        dd 0x00 
        dd - (0x1BADB002 + 0x00)

global start
global keyboard_handler
global read_port
global write_port
global load_idt

extern kernel_main
extern keyboard_handler_main

read_port:
	mov edx, [esp + 4]
  ;al is the lower 8 bits of eax
  ;dx is the lower 16 bits of edx
	in al, dx	
	ret

write_port:
	mov   edx, [esp + 4]    
	mov   al, [esp + 4 + 4]  
	out   dx, al  
	ret

load_idt:
	mov edx, [esp + 4]
	lidt [edx]
	sti ;turn on interrupts
	ret

keyboard_handler:                 
	call    keyboard_handler_main
	iretd

; Block interrupts, set the stack pointer, call the C entry point and halt the CPU
start:
	cli
	mov esp, stack_space
	call kernel_main
	hlt 

; Reserve 8KB stack space
section .bss
resb 8192; 8KB for stack
stack_space: