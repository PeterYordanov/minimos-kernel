#include "terminal.h"

void printk(const char* str)
{
    char *vidptr = (char*)0xb8000; //The start of the video memory
	unsigned int i = 0;
	unsigned int j = 0;

    //clrscr, 80 columns and 25 lines, each element takes 2 bytes, light grey on black screen
	while(j < 80 * 25 * 2) {
		vidptr[j] = ' ';
		vidptr[j + 1] = 0x07; 		
		j = j + 2;
	}

	j = 0;

	//Write str to video memory with black bacgrkound and light grey foreground, ASCII only
	while(str[j] != '\0') {
		vidptr[i] = str[j];
		vidptr[i + 1] = 0x07;
		++j;
		i = i + 2;
	}
}

void clear_screen(void)
{
}

void printk_hex(unsigned int key)
{
	char* foo = "00";
	char* hex = "0123456789ABCDEF";
	foo[0] = hex[(key >> 4) & 0xF];
	foo[1] = hex[key & 0xF];
	printk(foo);
}

void printk_hex16(unsigned int key)
{
	printk_hex((key >> 8) & 0xFF);
	printk_hex(key & 0xFF);
}

void printk_hex32(unsigned int key)
{
	printk_hex((key >> 24) & 0xFF);
	printk_hex((key >> 16) & 0xFF);
	printk_hex((key >> 8) & 0xFF);
	printk_hex(key & 0xFF);
}

void printk_hex64(unsigned int key)
{
	printk_hex((key >> 32) & 0xFF);
	printk_hex((key >> 24) & 0xFF);
	printk_hex((key >> 16) & 0xFF);
	printk_hex((key >> 8) & 0xFF);
	printk_hex(key & 0xFF);
}