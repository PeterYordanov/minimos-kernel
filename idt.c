#include "idt.h"
#include "keyboard_map.h"

void idt_init(void)
{
	unsigned long keyboard_address;
	unsigned long idt_address;
	unsigned long idt_ptr[2];

	// Populate IDT entry with the keyboard's interrupts
	keyboard_address = (unsigned long)keyboard_handler; 
	IDT[0x21].offset_lowerbits = keyboard_address & 0xffff;
	IDT[0x21].selector = 0x08; // Kernel code segment offset
	IDT[0x21].zero = 0;
	IDT[0x21].type_attr = 0x8e; // Interrupt gate
	IDT[0x21].offset_higherbits = (keyboard_address & 0xffff0000) >> 16;
	
	/*          Ports
	*	      PIC1	PIC2
	* Command 0x20	0xA0
	* Data	  0x21	0xA1
	*/

	// ICW1 - begin initialization
	write_port(0x20 , 0x11);
	write_port(0xA0 , 0x11);

	/* ICW2 - remap offset address of IDT
	 * In x86 protected mode, we have to remap the PICs beyond 0x20 because
	 * Intel have designated the first 32 interrupts as "reserved" for cpu exceptions
	 */
	write_port(0x21 , 0x20);
	write_port(0xA1 , 0x28);

	// ICW3 - setup cascading
	write_port(0x21 , 0x00);  
	write_port(0xA1 , 0x00);  

	// ICW4 - environment info
	write_port(0x21 , 0x01);
	write_port(0xA1 , 0x01);

	// Mask Interrupts
	write_port(0x21 , 0xff);
	write_port(0xA1 , 0xff);

	// Fill the Interrupt Descriptor Table 
	idt_address = (unsigned long)IDT ;
	idt_ptr[0] = (sizeof (struct idt_entry) * IDT_SIZE) + ((idt_address & 0xffff) << 16);
	idt_ptr[1] = idt_address >> 16 ;

	load_idt(idt_ptr);
}

void keyboard_init(void)
{
	/* 0xFD is 11111101 - enables only IRQ1 (keyboard)*/
	write_port(0x21 , 0xFD);
}

void keyboard_handler_main(void) {
	unsigned char status;
	char keycode;

	/* write EOI */
	write_port(0x20, 0x20);

	status = read_port(KEYBOARD_STATUS_PORT);
	/* Lowest bit of status will be set if buffer is not empty */
	if (status & 0x01) {
		keycode = read_port(KEYBOARD_DATA_PORT);
		if(keycode < 0)
			return;
		vidptr[current_loc++] = keyboard_map[keycode];
		vidptr[current_loc++] = 0x07;	
	}
}