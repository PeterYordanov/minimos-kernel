#ifndef TERMINAL_H
#define TERMINAL_H

#define LINES 25
#define COLUMNS_IN_LINE 80
#define BYTES_FOR_EACH_ELEMENT 2
#define SCREEN_SIZE BYTES_FOR_EACH_ELEMENT * COLUMNS_IN_LINE * LINES

void printk(const char* str);
void clear_screen(void);
void printk_hex(unsigned int key);
void printk_hex16(unsigned int key);
void printk_hex32(unsigned int key);
void printk_hex64(unsigned int key);

#endif // TERMINAL_H