SOURCES = boot.o main.o terminal.o idt.o

CFLAGS = -nostdlib -nostdinc -fno-builtin -fno-stack-protector -fno-stack-protector

LDFLAGS = -Tlink.ld
ASFLAGS = -felf

# Bulding rules
%.o: %.c
	gcc -m32 -c $< -o $@ 

%.o: %.asm
	nasm $(ASFLAGS) $<

# Make steps
all: $(SOURCES) link 

clean:
	-rm *.o kernel

link:
	ld -m elf_i386 $(LDFLAGS) -o kernel $(SOURCES)
